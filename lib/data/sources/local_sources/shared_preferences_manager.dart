import 'package:shared_preferences/shared_preferences.dart';

/// Created by HrAnt
/// Date: 08.02.23

SharedPreferences? _sharedPrefs;

Future<void> setSharedPreferencesInstance() async {
  _sharedPrefs ??= await SharedPreferences.getInstance();
}

SharedPreferences? get sharedPreferences => _sharedPrefs;
