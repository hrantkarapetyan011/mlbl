import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '/config/mlbl_colors.dart';
import '/data/models/player.dart';

/// Created by HrAnt
/// Date: 08.02.23

class PlayerInfo extends StatelessWidget {
  final Player player;

  const PlayerInfo({Key? key, required this.player}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 54.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: Text(
              player.number.toString(),
              style: GoogleFonts.lato(
                textStyle: Theme.of(context).textTheme.headlineMedium,
                fontSize: 18,
                color: darkGrey,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: player.avatar,
            ),
          ),
          Expanded(
            flex: 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  player.surname,
                  overflow: TextOverflow.fade,
                  maxLines: 1,
                  softWrap: false,
                  style: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.headlineMedium,
                    fontSize: 14.0,
                    color: darkGrey,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  player.name,
                  overflow: TextOverflow.fade,
                  maxLines: 1,
                  softWrap: false,
                  style: GoogleFonts.lato(
                    textStyle: Theme.of(context).textTheme.headlineMedium,
                    fontSize: 14.0,
                    color: darkGrey,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
