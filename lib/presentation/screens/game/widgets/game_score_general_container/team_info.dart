import 'package:flutter/cupertino.dart';

import '/config/mlbl_colors.dart';

/// Created by HrAnt
/// Date: 08.02.23

class TeamAvatarAndName extends StatelessWidget {
  final String teamName;
  final String teamAvatar;

  const TeamAvatarAndName({
    Key? key,
    required this.teamName,
    required this.teamAvatar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(teamAvatar),
        const SizedBox(
          height: 14.0,
        ),
        Text(
          teamName,
          style: const TextStyle(
            color: darkGrey,
          ),
        ),
      ],
    );
  }
}
