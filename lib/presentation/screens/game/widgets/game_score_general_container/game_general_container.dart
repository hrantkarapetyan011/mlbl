import 'package:flutter/material.dart';

import '/config/mlbl_colors.dart';
import '/presentation/widgets/flexible_container_with_borders.dart';
import 'game_match_parts_row.dart';
import 'game_matches_scores_row.dart';
import 'game_score_container.dart';

/// Created by HrAnt
/// Date: 08.02.23

class GameGeneralContainer extends StatelessWidget {
  const GameGeneralContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 18.0,
        left: 8.0,
        right: 8.0,
      ),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.32,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0.0, 2.0),
            ),
          ],
        ),
        child: Column(
          children: const [
            FlexibleContainerWithBorders(
              flex: 3,
              child: GameScoreContainer(),
            ),
            FlexibleContainerWithBorders(
              topBorder: true,
              borderColor: darkGrey,
              child: MatchPartsRow(),
            ),
            FlexibleContainerWithBorders(
              topBorder: true,
              borderColor: darkGrey,
              child: MatchesScoresRow(
                scores: [16, 19, 14, 17],
              ),
            ),
            FlexibleContainerWithBorders(
              topBorder: true,
              child: MatchesScoresRow(
                scores: [16, 13, 15, 19],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
