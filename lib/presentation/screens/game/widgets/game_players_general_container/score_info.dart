import 'package:flutter/cupertino.dart';

import '/data/models/score.dart';
import '/presentation/screens/game/widgets/game_players_general_container/score_item.dart';

/// Created by HrAnt
/// Date: 08.02.23

class ScoreInfo extends StatelessWidget {
  final Score score;

  const ScoreInfo({
    Key? key,
    required this.score,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 54.0,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 12.0,
          vertical: 2.0,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ScoreItem(
              value: score.min,
            ),
            ScoreItem(
              value: score.o.toString(),
            ),
            ScoreItem(
              value: score.twoScore,
            ),
            ScoreItem(
              value: score.threeScore,
            ),
            ScoreItem(
              value: score.shb.toString(),
            ),
            ScoreItem(
              value: score.psh.toString(),
            ),
            ScoreItem(
              value: score.pt.toString(),
            ),
            ScoreItem(
              value: score.fs.toString(),
            ),
            ScoreItem(
              value: score.plusMinus.toString(),
            ),
            ScoreItem(
              value: score.kpi.toString(),
            ),
          ],
        ),
      ),
    );
  }
}
