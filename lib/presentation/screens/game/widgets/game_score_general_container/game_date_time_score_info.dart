import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '/config/mlbl_colors.dart';

/// Created by HrAnt
/// Date: 08.02.23

class GameDateTimeScoreInfo extends StatelessWidget {
  final List<int> scores;
  final String date;
  final String time;

  const GameDateTimeScoreInfo({
    Key? key,
    required this.scores,
    required this.date,
    required this.time,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          '${scores.first}',
          style: GoogleFonts.lato(
            textStyle: Theme.of(context).textTheme.headlineMedium,
            fontSize: 24,
            fontWeight: FontWeight.w300,
          ),
        ),
        const SizedBox(
          height: 26.0,
          child: VerticalDivider(
            width: 24.0,
            thickness: 2,
            color: lightGrey,
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              date,
              style: const TextStyle(
                color: darkGrey,
              ),
            ),
            const SizedBox(
              height: 12.0,
            ),
            Text(
              time,
              style: const TextStyle(
                color: Colors.lightBlue,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 26.0,
          child: VerticalDivider(
            width: 24.0,
            thickness: 2,
            color: lightGrey,
          ),
        ),
        Text(
          '${scores.last}',
          style: GoogleFonts.lato(
            textStyle: Theme.of(context).textTheme.headlineMedium,
            fontSize: 24,
            fontWeight: FontWeight.w300,
          ),
        ),
      ],
    );
  }
}
