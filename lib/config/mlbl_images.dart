/// Created by HrAnt 
/// Date: 08.02.23

const String gameImage = 'assets/images/game_logo.png';
const String teamOne = 'assets/images/team1.png';
const String teamTwo = 'assets/images/team2.png';

const String avatarOne = 'assets/images/avatar1.png';
const String avatarTwo = 'assets/images/avatar2.png';
const String avatarThree = 'assets/images/avatar3.png';
const String avatarFour = 'assets/images/avatar4.png';
const String avatarFive = 'assets/images/avatar5.png';
const String avatarSix = 'assets/images/avatar6.png';
const String avatarSeven = 'assets/images/avatar7.png';
const String avatarEight = 'assets/images/avatar8.png';