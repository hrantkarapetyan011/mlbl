import 'package:flutter/cupertino.dart';

import '/config/mlbl_colors.dart';

/// Created by HrAnt
/// Date: 08.02.23

class MatchesScoresRow extends StatelessWidget {
  final List<int> scores;

  const MatchesScoresRow({
    Key? key,
    required this.scores,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 42.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: scores
            .map(
              (i) => Text(
                i.toString(),
                style: const TextStyle(
                  fontSize: 16.0,
                  color: darkGrey,
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
