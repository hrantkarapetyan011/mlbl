import 'package:flutter/cupertino.dart';
import 'package:mlbl/config/mlbl_colors.dart';

import '/config/mlbl_strings.dart';

/// Created by HrAnt
/// Date: 08.02.23

class MatchPartsRow extends StatelessWidget {
  const MatchPartsRow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: matchParts
          .map(
            (i) => Text(
              i.toString(),
              style: const TextStyle(
                fontSize: 14.0,
                color: darkGrey,
              ),
            ),
          )
          .toList(),
    );
  }
}
