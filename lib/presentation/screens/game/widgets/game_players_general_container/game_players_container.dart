import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '/config/mlbl_colors.dart';
import '/config/mlbl_strings.dart';
import '/presentation/screens/game/widgets/game_players_general_container/first_team.dart';
import '/presentation/screens/game/widgets/game_players_general_container/second_team.dart';

/// Created by HrAnt
/// Date: 08.02.23

class PlayersContainer extends StatelessWidget {
  const PlayersContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 22.0,
      ),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.6,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 5,
                blurRadius: 7,
                offset: const Offset(0.0, 2.0),
              ),
            ],
          ),
          child: DefaultTabController(
            length: 2,
            child: Scaffold(
              appBar: TabBar(
                labelColor: Colors.black,
                unselectedLabelColor: darkGrey,
                indicatorColor: lightBlue,
                indicatorWeight: 5.0,
                tabs: [
                  Tab(
                    child: Text(
                      mgpu2,
                      style: GoogleFonts.lato(
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      mba,
                      style: GoogleFonts.lato(
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                ],
              ),
              body: const TabBarView(
                physics: NeverScrollableScrollPhysics(),
                children: [
                  FirstTeam(),
                  SecondTeam(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
