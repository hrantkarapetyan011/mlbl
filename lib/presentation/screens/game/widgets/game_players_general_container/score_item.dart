import 'package:flutter/cupertino.dart';

/// Created by HrAnt
/// Date: 08.02.23

class ScoreItem extends StatelessWidget {
  final String value;

  const ScoreItem({
    Key? key,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Text(
          value,
        ),
      ),
    );
  }
}
