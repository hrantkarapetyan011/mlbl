import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '/presentation/screens/game/widgets/game_players_general_container/players_column.dart';
import '/presentation/screens/game/widgets/game_players_general_container/scores_column.dart';

/// Created by HrAnt
/// Date: 08.02.23

class SecondTeam extends StatelessWidget {
  const SecondTeam({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: const [
        PlayersColumn(),
        ScoresColumn(),
      ],
    );
  }
}
