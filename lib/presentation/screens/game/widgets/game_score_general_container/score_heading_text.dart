import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

/// Created by HrAnt
/// Date: 08.02.23

class ScoreHeadingText extends StatelessWidget {
  final String title;

  const ScoreHeadingText({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: GoogleFonts.lato(
        textStyle: Theme.of(context).textTheme.headlineMedium,
        fontSize: 16,
        fontWeight: FontWeight.w300,
      ),
    );
  }
}
