import 'package:flutter/material.dart';

import '/config/mlbl_colors.dart';
import '/config/mlbl_constants.dart';
import '/config/mlbl_strings.dart';
import '/presentation/screens/game/widgets/game_players_general_container/player_info.dart';
import '/presentation/screens/game/widgets/game_score_general_container/score_heading_text.dart';

/// Created by HrAnt
/// Date: 08.02.23

class PlayersColumn extends StatelessWidget {
  const PlayersColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      decoration: const BoxDecoration(
        border: Border(
          right: BorderSide(
            color: lightGrey,
            width: 1.0,
          ),
        ),
      ),
      child: DataTable(
        horizontalMargin: 12.0,
        dataRowHeight: 54.0,
        showBottomBorder: true,
        columns: const [
          DataColumn(
            label: ScoreHeadingText(
              title: gamer,
            ),
          ),
        ],
        rows: players
            .map(
              (e) => DataRow(
                cells: [
                  DataCell(
                    PlayerInfo(
                      player: e,
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }
}
