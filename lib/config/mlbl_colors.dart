import 'dart:ui';

/// Created by HrAnt
/// Date: 08.02.23

const Color darkBlue = Color(0xFF00325E);
const Color lightBlue = Color(0xFF005E8A);
const Color orange = Color(0xFFE95226);
const Color darkGrey = Color(0xff484a4f);
const Color lightGrey = Color(0xFFe5e5e5);