import 'package:flutter/material.dart';

import '/config/mlbl_constants.dart';
import '/presentation/screens/game/widgets/game_score_general_container/score_heading_text.dart';

/// Created by HrAnt
/// Date: 08.02.23

class ScoresColumn extends StatelessWidget {
  const ScoresColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          dataRowHeight: 54.0,
          showBottomBorder: true,
          columns: const [
            DataColumn(
              label: ScoreHeadingText(
                title: 'Мин',
              ),
            ),
            DataColumn(
              label: ScoreHeadingText(
                title: 'О',
              ),
            ),
            DataColumn(
              label: ScoreHeadingText(
                title: '2-очки',
              ),
            ),
            DataColumn(
              label: ScoreHeadingText(
                title: '3-очки',
              ),
            ),
            DataColumn(
              label: ScoreHeadingText(
                title: 'ШБ',
              ),
            ),
            DataColumn(
              label: ScoreHeadingText(
                title: 'ПШ',
              ),
            ),
            DataColumn(
              label: ScoreHeadingText(
                title: 'ПТ',
              ),
            ),
            DataColumn(
              label: ScoreHeadingText(
                title: 'ФС',
              ),
            ),
            DataColumn(
              label: ScoreHeadingText(
                title: '+/-',
              ),
            ),
            DataColumn(
              label: ScoreHeadingText(
                title: 'КПИ',
              ),
            ),
          ],
          rows: scores
              .map(
                (e) => DataRow(
                  cells: [
                    DataCell(
                      Text(
                        e.min.toString(),
                      ),
                    ),
                    DataCell(
                      Text(
                        e.o.toString(),
                      ),
                    ),
                    DataCell(
                      Text(
                        e.twoScore.toString(),
                      ),
                    ),
                    DataCell(
                      Text(
                        e.threeScore.toString(),
                      ),
                    ),
                    DataCell(
                      Text(
                        e.shb.toString(),
                      ),
                    ),
                    DataCell(
                      Text(
                        e.psh.toString(),
                      ),
                    ),
                    DataCell(
                      Text(
                        e.pt.toString(),
                      ),
                    ),
                    DataCell(
                      Text(
                        e.fs.toString(),
                      ),
                    ),
                    DataCell(
                      Text(
                        e.plusMinus.toString(),
                      ),
                    ),
                    DataCell(
                      Text(
                        e.kpi.toString(),
                      ),
                    ),
                  ],
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
