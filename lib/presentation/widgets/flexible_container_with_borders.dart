import 'package:flutter/cupertino.dart';

import '/config/mlbl_colors.dart';

/// Created by HrAnt
/// Date: 08.02.23

class FlexibleContainerWithBorders extends StatelessWidget {
  final int flex;
  final Color borderColor;
  final bool topBorder, bottomBorder;
  final Widget child;

  const FlexibleContainerWithBorders({
    Key? key,
    required this.child,
    this.borderColor = lightGrey,
    this.topBorder = false,
    this.bottomBorder = false,
    this.flex = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            top: topBorder
                ? BorderSide(
                    color: borderColor,
                    width: 1.0,
                  )
                : BorderSide.none,
            bottom: bottomBorder
                ? BorderSide(
                    color: borderColor,
                    width: 1.0,
                  )
                : BorderSide.none,
          ),
        ),
        child: child,
      ),
    );
  }
}
