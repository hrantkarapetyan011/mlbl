import 'package:flutter/material.dart';

import '/config/mlbl_colors.dart';
import '/config/mlbl_images.dart';
import '/config/mlbl_strings.dart';
import 'game_date_time_score_info.dart';
import 'team_info.dart';

/// Created by HrAnt
/// Date: 08.02.23

class GameScoreContainer extends StatelessWidget {
  const GameScoreContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.symmetric(
            vertical: 12.0,
          ),
          child: Text(
            scoreContainerTitle,
            style: TextStyle(
              fontSize: 16.0,
              color: darkGrey,
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const [
            TeamAvatarAndName(
              teamName: mgpu2,
              teamAvatar: teamOne,
            ),
            GameDateTimeScoreInfo(
              date: '08.02.2023',
              time: '22:00',
              scores: [77, 88],
            ),
            TeamAvatarAndName(
              teamName: mba,
              teamAvatar: teamTwo,
            ),
          ],
        ),
      ],
    );
  }
}
