import 'package:flutter/cupertino.dart';

/// Created by HrAnt
/// Date: 08.02.23

class Player {
  int number;
  Widget avatar;
  String surname;
  String name;

  Player(this.number, this.avatar, this.surname, this.name);
}
