/// Created by HrAnt
/// Date: 08.02.23

class Score {
  Score(this.min, this.o, this.twoScore, this.threeScore, this.shb, this.psh,
      this.pt, this.fs, this.plusMinus, this.kpi);

  String min;
  String twoScore;
  String threeScore;
  int o;
  int shb;
  int psh;
  int pt;
  int fs;
  int plusMinus;
  int kpi;
}
