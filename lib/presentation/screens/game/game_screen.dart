import 'package:flutter/material.dart';

import '/config/mlbl_colors.dart';
import '/config/mlbl_images.dart';
import '/config/mlbl_strings.dart';
import 'widgets/game_score_general_container/game_general_container.dart';
import 'widgets/game_players_general_container/game_players_container.dart';
import '/presentation/widgets/m_l_b_l_icon_icons.dart';

/// Created by HrAnt
/// Date: 08.02.23

class GameScreen extends StatefulWidget {
  const GameScreen({super.key});

  @override
  State<GameScreen> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<GameScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: darkBlue,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(
            Icons.arrow_back_ios,
          ),
        ),
        centerTitle: true,
        title: const Text(
          game,
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(
              MLBLIcon.share,
              color: Colors.white,
              size: 24.0,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8.0,
                vertical: 22.0,
              ),
              child: Image.asset(
                gameImage,
              ),
            ),
            const GameGeneralContainer(),
            const PlayersContainer(),
          ],
        ),
      ),
    );
  }
}
