import 'package:flutter/material.dart';

import '/data/models/player.dart';
import '/data/models/score.dart';
import 'mlbl_images.dart';

/// Created by HrAnt
/// Date: 08.02.23

final List<Player> players = [
  Player(22, Image.asset(avatarOne), 'Ровчанин', 'Адмир'),
  Player(1, Image.asset(avatarTwo), 'Потапкин', 'Иван'),
  Player(11, Image.asset(avatarThree), 'Зарьянов', 'Никита'),
  Player(23, Image.asset(avatarFour), 'Яхунин', 'Сергей'),
  Player(7, Image.asset(avatarFive), 'Ермаков', 'Антон'),
  Player(2, Image.asset(avatarSix), 'Терехин', 'Станислав'),
  Player(15, Image.asset(avatarSeven), 'Андреев', 'Даниил'),
  Player(77, Image.asset(avatarEight), 'Дедушкин', 'Алекс'),
];

final List<Score> scores = [
  Score('22:20', 8, '68%', '54%', 12, 6, 87, 56, -1, 9),
  Score('13:33', 3, '54%', '54%', 12, 6, 87, 56, -1, 9),
  Score('10:20', 4, '34%', '54%', 12, 6, 87, 56, -1, 9),
  Score('11:20', 5, '64%', '54%', 12, 6, 87, 56, -1, 9),
  Score('12:43', 8, '23%', '54%', 12, 6, 87, 56, -1, 9),
  Score('16:56', 6, '44%', '54%', 12, 6, 87, 56, -1, 9),
  Score('23:11', 1, '78%', '54%', 12, 6, 87, 56, -1, 9),
  Score('12:00', 2, '56%', '54%', 12, 6, 87, 56, -1, 9),
];
